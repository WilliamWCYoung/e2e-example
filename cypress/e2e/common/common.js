const {
  Given,
  getParameterValue,
  When,
} = require("arborist-cypress-plugin");

When("Go to {url}", (props) => {
  const url = getParameterValue(props.url);
  cy.visit(Cypress.env("base_url") + url);
});

Given("Currently at {url}", (props) => {
  const url = getParameterValue(props.url);
  cy.url().should("match", RegExp(Cypress.env("base_url") + url));
});

When("Select the input", (props) => {
    cy.get(".new-todo").click().click();
});

When("Type {content}", (props) => {
  const content = getParameterValue(props.content);
  cy.focused().type(content);
});

When("Press enter", (props) => {
  cy.focused().type("{enter}");
});

Given("There is an item with {content}", (props) => {
  const content = getParameterValue(props.content);
  cy.contains(content).should("exist", 1);
});
