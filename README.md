# Arborist E2E example

To run the tree traversal test, use the following command:
```
npx cypress run --env project_id=<PROJECT_ID>,runner_token=<RUNNER_TOKEN>
```