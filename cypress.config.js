const { defineConfig } = require("cypress");
const { submitNode } = require("arborist-cypress-plugin");

module.exports = defineConfig({
  env: {
    export_base_url: "https://api.arborist.dev",
    base_url: "https://todomvc.com/examples/typescript-react/#/",
    project_id: null,
    runner_token: null,
  },
  e2e: {
    supportFile: false,
    experimentalSessionAndOrigin: true,
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      // return require('./cypress/plugins/index.js')(on, config)
      on("after:screenshot", (details) => {
        submitNode(config, details);
      });
    },
  },
  // specPattern: 'cypress/e2e/**/*.{feature,features}',
});
